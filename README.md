# ModIoCleaner:

A program which deletes all your files in the .Modio folder so you dont have to do it manually.

## State of development:

The first running version is finished. It is not yet fully battletested but should work correctly.
If you run into an error pls make an issue with the error message attached or contact me on discord.

## Requirements:

It requires .Net Framework 4.8 to be installed. 

## Current features: 
 - Deleting mod io folder
 - Starting mordhau after finishing deleting mod io folder

## Planned features:
 - Validating game files
