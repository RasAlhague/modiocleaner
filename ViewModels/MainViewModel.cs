﻿using ModIoCleaner.Commands;
using System.IO;
using System.Windows.Input;
using Ookii.Dialogs.Wpf;
using ModIoCleaner.Lib;
using ModIoCleaner.Lib.Models;

namespace ModIoCleaner.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private bool _startMordhau;
        private string _modIoPath;
        private string _logText;
        private readonly ModCleaner _modIoCleaner;
        private bool _isCleaning;

        public bool StartMordhau { get => _startMordhau; set => SetProperty(ref _startMordhau, value); }
        public bool IsCleaning { get => _isCleaning; set => SetProperty(ref _isCleaning, value); }
        public string ModIoPath { get => _modIoPath; set => SetProperty(ref _modIoPath, value); }
        public string LogText { get => _logText; set => SetProperty(ref _logText, value); }

        public ICommand StartCommand { get; }
        public ICommand SaveSettingsCommand { get; }
        public ICommand SelectPathCommand { get; }

        public MainViewModel()
        {
            string settingsPath = SettingsModel.GetSettingsFolderPath();

            if (!Directory.Exists(SettingsModel.GetSettingsFolderPath()))
            {
                Directory.CreateDirectory(settingsPath);
            }

            var settingsModel = SettingsModel.Load(SettingsModel.GetSettingsFilePath());

            StartMordhau = settingsModel.StartMordhauAfterCleanup;
            ModIoPath = settingsModel.ModIoPath;

            StartCommand = new RelayCommand(StartCommandExecute, StartCommandCanExecute);
            SaveSettingsCommand = new RelayCommand(SaveSettingsCommandExecute, SaveSettingsCommandCanExecute);
            SelectPathCommand = new RelayCommand(SelectPathCommandExecute, SelectPathCommandCanExecute);

            _modIoCleaner = new ModCleaner(settingsModel.StartMordhauAfterCleanup, settingsModel.ModIoPath, settingsModel.GameStartId, settingsModel.SteamStartPath);
            _modIoCleaner.LogMessageSent += LogMessageHandler;
            _isCleaning = false;
            _logText = string.Empty;
        }

        private void LogMessageHandler(string message)
        {
            LogText += $"\n{message}";
        }

        private void SelectPathCommandExecute(object obj)
        {
            var dialog = new VistaFolderBrowserDialog
            {
                Description = "Select the modio path."
            };

            if (dialog.ShowDialog().Value)
            {
                ModIoPath = dialog.SelectedPath;
            }
        }

        private bool SelectPathCommandCanExecute(object arg)
        {
            return !IsCleaning;
        }

        private bool SaveSettingsCommandCanExecute(object arg)
        {
            return !IsCleaning;
        }

        private void SaveSettingsCommandExecute(object obj)
        {
            var settings = new SettingsModel
            {
                StartMordhauAfterCleanup = StartMordhau,
                ModIoPath = ModIoPath
            };

            string settingsPath = SettingsModel.GetSettingsFolderPath();

            if(!Directory.Exists(settingsPath))
            {
                Directory.CreateDirectory(settingsPath);
            }

            SettingsModel.Save(SettingsModel.GetSettingsFilePath(), settings);

            _modIoCleaner.ModIoPath = ModIoPath;
            _modIoCleaner.RestartMordhau = StartMordhau;
        }

        private bool StartCommandCanExecute(object arg)
        {
            return !IsCleaning;
        }

        private void StartCommandExecute(object obj)
        {
            IsCleaning = true;

            _modIoCleaner.ModIoPath = ModIoPath;
            _modIoCleaner.RestartMordhau = StartMordhau;
            _modIoCleaner.Run();

            IsCleaning = false;
        }
    }
}
